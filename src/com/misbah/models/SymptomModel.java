package com.misbah.models;

/**
 * Created by misba on 5/27/2017.
 */
public class SymptomModel {
    private String name;
    private String question;
    private boolean isTrue;

    public SymptomModel() {
    }

    public SymptomModel(String name, boolean isTrue) {
        this.name = name;
        this.isTrue = isTrue;
    }

    public SymptomModel(String name, String question, boolean isTrue) {
        this.name = name;
        this.question = question;
        this.isTrue = isTrue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public void setIsTrue(boolean isTrue) {
        this.isTrue = isTrue;
    }
}
