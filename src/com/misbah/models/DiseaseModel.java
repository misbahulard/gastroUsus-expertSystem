package com.misbah.models;

import java.util.ArrayList;

/**
 * Created by misba on 5/27/2017.
 */
public class DiseaseModel extends SymptomModel {
    private ArrayList<Integer> diseaseList;
    private double probability;

    public DiseaseModel() {
    }

    public DiseaseModel(ArrayList<Integer> diseaseList) {
        this.diseaseList = diseaseList;
    }

    public DiseaseModel(ArrayList<Integer> diseaseList, String name, boolean isTrue) {
        super(name, isTrue);
        this.diseaseList = diseaseList;
    }

    public ArrayList<Integer> getDiseaseList() {
        return diseaseList;
    }

    public void setDiseaseList(ArrayList<Integer> diseaseList) {
        this.diseaseList = diseaseList;
    }

    public double getProbability() {
        return probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }
}
