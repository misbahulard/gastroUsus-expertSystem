package com.misbah.main;

import com.misbah.models.DiseaseModel;
import com.misbah.models.SymptomModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by misba on 5/27/2017.
 */
public class MainProgram {
    // private static DiseaseModel root;
    private static ArrayList<DiseaseModel> child1, child2;
    private static ArrayList<SymptomModel> symptoms;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Double> child2Value= new ArrayList<>();
        ArrayList<Double> child1Value = new ArrayList<>();
        String answer;
        double threshold = 0;
        boolean isTrue = false;
        int count = 0;
        int symptomSize = 0;
        double result = 0;

        symptoms = initializeSymtom();
        child2 = initializeChild2();
        child1 = initializeChild1();
        // root = initializeRoot();

        System.out.println("Sistem Pakar Gastro-usus\n");
        try {
            for (int i = 0; i < symptoms.size(); i++) {
                System.out.print((i+1) + " " + symptoms.get(i).getQuestion() + "? ");
                answer = in.readLine();
                if (answer.equalsIgnoreCase("y")) {
                    symptoms.get(i).setIsTrue(true);
                }
            }

            System.out.println("\nThreshold: ");
            answer = in.readLine();
            threshold = Double.valueOf(answer);
            System.out.println("=================================\n");
        } catch (IOException e) {
            e.printStackTrace();
        }


        /**
         * Debug process
         */
//        for (int i = 0; i < symptoms.size(); i++) {
//            System.out.println(i + ": " + symptoms.get(i).isTrue());
//        }

        /**
         * Set child2 to true if the child contain at least one symptom
         * And calculate the amount of symptom
         */


        for (int i = 0; i < child2.size(); i++) {
            symptomSize = child2.get(i).getDiseaseList().size();
            for (int j = 0; j < symptoms.size(); j++) {
                isTrue = symptoms.get(j).isTrue();
                if (isTrue && child2.get(i).getDiseaseList().contains(j)) {
                    count++;
                }
            }
            result = (double) count / symptomSize * 100;
            child2.get(i).setProbability(result);
            if (result >= threshold) {
                child2.get(i).setIsTrue(true);
            }

            count = 0;
        }

        /**
         * Debug process
         */
//        System.out.println("Child2");
//        for (int i = 0; i < child2.size(); i++) {
//            System.out.println(i + ": " + child2.get(i).getProbability());
//        }
//        System.out.println("");
//        for (int i = 0; i < child2.size(); i++) {
//            System.out.println(i + ": " + child2.get(i).isTrue());
//        }
//        System.out.println("");

        /**
         * Set child1 to true if the child contain at least one disease
         * And calculate the amount of disease
         */
        isTrue = false;
        count = 0;
        symptomSize = 0;
        result = 0;

        for (int i = 0; i < child1.size(); i++) {
            symptomSize = child1.get(i).getDiseaseList().size();
            for (int k = 0; k < child2.size(); k++) {
                if (child1.get(i).getDiseaseList().contains(k) && child2.get(k).isTrue()) {
                    count++;
                }
            }
            result = (double) count / symptomSize * 100;
            child1.get(i).setProbability(result);
            if (result >= threshold) {
                child1.get(i).setIsTrue(true);
            }

            count = 0;
        }

        /**
         * Print the result
         */

        System.out.println("Kemungkinan Anda terkena: ");

        for (int i = 0; i < child1.size(); i++) {
            System.out.format("%.0f", child1.get(i).getProbability());
            System.out.println("% Keracunan " + child1.get(i).getName());
            System.out.println("Dengan gejala: ");
            for (int k = 0; k < child2.size(); k++) {
                if (child1.get(i).getDiseaseList().contains(k) && child2.get(k).isTrue() && child1.get(i).getProbability() >= 0.0) {
                    System.out.print("- " + child2.get(k).getName() + " ");
                    System.out.format("( %.0f%% )%n", child2.get(k).getProbability());
                }
            }
            System.out.println("");
        }

    }

    public static ArrayList<SymptomModel> initializeSymtom(){
        ArrayList<SymptomModel> symtomList = new ArrayList<>();
        SymptomModel symtom;

        symtom = new SymptomModel("Buang air besar", "Apakah anda sering mengalami buang air besar (lebih dari 2 kali)", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Berak encer", "Apakah anda mengalami berak encer", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Berak berdarah", "Apakah anda mengalami berak berdarah", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Lesu dan tidak bergairah", "Apakah anda merasa lesu dan tidak bergairah", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Tidak selera makan", "Apakah anda tidak selera makan", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Merasa mual dan sering muntah", "Apakah anda merasa mual dan sering muntah (lebih dari 1 kali)", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Merasa sakit di bagian perut", "Apakah anda merasa sakit di bagian perut", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Tekanan darah rendah", "Apakah tekanan darah anda rendah", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Pusing", "Apakah tekanan darah anda rendah", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Pingsan", "Apakah anda merasa pusing", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Suhu badan tinggi", "Apakah anda mengalami pingsan", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Luka di bagian tertentu", "Apakah suhu badan anda tinggi", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Tidak dapat menggerakkan anggota badan tertentu", "Apakah anda tidak mengalami luka di bagian tertentu", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Memakan sesuatu", "Apakah anda pernah memakan sesuatu", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Memakan daging", "Apakah anda memakan daging", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Memakan jamur", "Apakah anda memakan jamur", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Memakan makanan kaleng", "Apakah anda memakan makanan kaleng", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Membeli susu", "Apakah anda membeli susu", false);
        symtomList.add(symtom);
        symtom = new SymptomModel("Meminum susu", "Apakah anda meminum susu", false);
        symtomList.add(symtom);

        return symtomList;
    }

    public static ArrayList<DiseaseModel> initializeChild1(){
        ArrayList<DiseaseModel> diseaseList = new ArrayList<>();
        ArrayList<Integer> symList = new ArrayList<>();
        DiseaseModel disease;

        symList.add(0);
        symList.add(1);
        symList.add(2);
        symList.add(3);
        symList.add(9);
        disease = new DiseaseModel(symList, "Staphylococcus aureus", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(0);
        symList.add(1);
        symList.add(2);
        symList.add(4);
        symList.add(10);
        disease = new DiseaseModel(symList, "Jamur beracun", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(0);
        symList.add(1);
        symList.add(2);
        symList.add(5);
        symList.add(6);
        symList.add(9);
        disease = new DiseaseModel(symList, "Salmonellae", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(1);
        symList.add(7);
        symList.add(11);
        disease = new DiseaseModel(symList, "Clostridium botulinum", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(8);
        symList.add(2);
        symList.add(5);
        symList.add(12);
        disease = new DiseaseModel(symList, "Campylobacter", false);
        diseaseList.add(disease);

        return diseaseList;
    }

    public static ArrayList<DiseaseModel> initializeChild2(){
        ArrayList<DiseaseModel> diseaseList = new ArrayList<>();
        ArrayList<Integer> symList = new ArrayList<>();
        DiseaseModel disease;

        symList.add(0);
        symList.add(1);
        symList.add(3);
        symList.add(4);
        disease = new DiseaseModel(symList, "Mencret", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(3);
        symList.add(4);
        symList.add(5);
        disease = new DiseaseModel(symList, "Muntah", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(3);
        symList.add(6);
        disease = new DiseaseModel(symList, "Sakit Perut", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(3);
        symList.add(7);
        symList.add(8);
        disease = new DiseaseModel(symList, "Darah Rendah", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(7);
        symList.add(9);
        disease = new DiseaseModel(symList, "Koma", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(3);
        symList.add(4);
        symList.add(8);
        symList.add(10);
        disease = new DiseaseModel(symList, "Demam", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(3);
        symList.add(7);
        symList.add(10);
        symList.add(11);
        disease = new DiseaseModel(symList, "Septicaemia", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(3);
        symList.add(12);
        disease = new DiseaseModel(symList, "Lumpuh", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(0);
        symList.add(1);
        symList.add(2);
        symList.add(3);
        symList.add(4);
        disease = new DiseaseModel(symList, "Mencret berdarah", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(13);
        symList.add(14);
        disease = new DiseaseModel(symList, "Makan daging", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(13);
        symList.add(15);
        disease = new DiseaseModel(symList, "Makan jamur", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(13);
        symList.add(16);
        disease = new DiseaseModel(symList, "Makan makanan kaleng", false);
        diseaseList.add(disease);

        symList = new ArrayList<>();
        symList.add(17);
        symList.add(18);
        disease = new DiseaseModel(symList, "Minum susu", false);
        diseaseList.add(disease);

        return diseaseList;
    }

    public static DiseaseModel initializeRoot() {
        ArrayList<Integer> symList = new ArrayList<>();

        symList.add(0);
        symList.add(1);
        symList.add(2);
        symList.add(3);
        symList.add(4);
        DiseaseModel dm = new DiseaseModel(symList, "root", false);

        return dm;
    }
}
